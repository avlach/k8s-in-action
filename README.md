## Minikube in Action 

Single node cluster using Minikube to familiarize yourself with Kubernetes.

### Docker warm up

- Run node app that returns container hostname
~~~
docker run -p 8080:8080 -d tasosvl/kubia
docker ps
docker stop {container name}
~~~

### Minikube getting started

- Start minikube single node K8s cluster (requires minikube, kubectl & virtualbox installed). 
- Start node app using public container created in previous step.
- Change the replication factor to 3 (number of application pods)
~~~
minikube start
minikube dashboard

kubectl run kubia --image=tasosvl/kubia --port=8080 --generator=run/v1
kubectl expose rc kubia --type=LoadBalancer --name kubia-http
minikube service kubia-http     # minikube specific step (opens service URL to browser)

kubectl get svc
kubectl get rc
kubectl scale rc kubia --replicas=3
kubectl get rc
kubectl get pods

kubectl exec -it {pod name} /bin/bash
~~~

### Kubectl with YAML files instead

- Working with YAML files instead to create a pod
- check pod logs (specifying the container name if required)
- enable kubectl port-forwarding process (for debugging only) to the local machine
~~~
kubectl get pods {pod name} -o yaml
kubectl create -f pods/kubia-manual.yaml

kubectl create -f pods/kubia-manual.yaml
kubectl logs kubia-manual [-c kubia]

kubectl port-forward kubia-manual 8888:8080
curl localhost:8888
~~~

### Use labels to organize your resources

- modify labels of runnning pods 
- show labels when listing resources
- filter resources based on label
~~~
kubectl label pods {pod-name} {label_key}={label_value} [--overwrite]
kubectl get po -L {label.key},{another_label.key}
kubectl get po -l {label.key}={label.value}
~~~

### Delete ReplicationController but keep pods intact
 - possibly you want to delete your RC in favour of a ReplicaSet (RS)
 - RS are similar to RC but they have more expresive pod selectors 
~~~
kubectl delete rc kubia --cascade=false
~~~

### Use Jobs to run a process once
- Job can be configured to restart pod or not
- on node failure Job will restart the container on a different node
~~~

~~~

### Disable role based access control 
- service accounts normally should not have access to all parts of server API 
- disable RBAC for any service account on your testing cluster using the following command
~~~
kubectl create clusterrolebinding permissive-binding \
  --clusterrole=cluster-admin \
  --group=system:serviceaccounts
~~~
