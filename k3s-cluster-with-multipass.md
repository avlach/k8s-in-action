# Create a 3 node cluster using multipass & k3s


### Configure multipass to use libvirt 

- optional step, helps to have more control over the VMs
~~~
sudo apt install libvirt-daemon-system

multipass stop --all

sudo multipass set local.driver=libvirt
# switch back to the qemu driver (if you face any problems)
sudo multipass set local.driver=qemu
~~~

### Create 3 ubuntu VMs and install a 3 node K8s cluster using k3s

~~~
for node in kube-master kube-node01 kube-node02;do
  multipass launch -n $node
done

# Init cluster on kube-master
multipass exec kube-master -- bash -c "curl -sfL https://get.k3s.io | sh -"

# Get kube-master's IP
IP=$(multipass info kube-master | grep IPv4 | awk '{print $2}')

# Get Token used to join nodes
TOKEN=$(multipass exec kube-master sudo cat /var/lib/rancher/k3s/server/node-token)

# Join kube-node01
multipass exec kube-node01 -- \
bash -c "curl -sfL https://get.k3s.io | K3S_URL=\"https://$IP:6443\" K3S_TOKEN=\"$TOKEN\" sh -"

# Join kube-node02
multipass exec kube-node02 -- \
bash -c "curl -sfL https://get.k3s.io | K3S_URL=\"https://$IP:6443\" K3S_TOKEN=\"$TOKEN\" sh -"

# Get cluster's configuration
multipass exec kube-master sudo cat /etc/rancher/k3s/k3s.yaml > k3s.yaml
~~~

# Gotchas

Delete node password if you are deleting/adding node and it cannot connect to the cluster.

~~~
multipass exec kube-master  -- sudo vi /var/lib/rancher/k3s/server/cred/node-passwd
~~~

