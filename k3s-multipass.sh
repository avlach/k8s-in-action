for node in kube-master kube-node01 kube-node02;do
  multipass launch -n $node
done

# Init cluster on kube-master
multipass exec kube-master -- bash -c "curl -sfL https://get.k3s.io | sh -"

# Get kube-master's IP
IP=$(multipass info kube-master | grep IPv4 | awk '{print $2}')

# Get Token used to join nodes
TOKEN=$(multipass exec kube-master sudo cat /var/lib/rancher/k3s/server/node-token)

# Join kube-node01
multipass exec kube-node01 -- \
bash -c "curl -sfL https://get.k3s.io | K3S_URL=\"https://$IP:6443\" K3S_TOKEN=\"$TOKEN\" sh -"

# Join kube-node02
multipass exec kube-node02 -- \
bash -c "curl -sfL https://get.k3s.io | K3S_URL=\"https://$IP:6443\" K3S_TOKEN=\"$TOKEN\" sh -"

# Get cluster's configuration
multipass exec kube-master sudo cat /etc/rancher/k3s/k3s.yaml > k3s.yaml

# Set kube-master's external IP in the configuration file
sed -i '' "s/127.0.0.1/$IP/" k3s.yaml

# We'r all set
echo
echo "K3s cluster is ready !"
echo
echo "Run the following command to set the current context:"
echo "$ export KUBECONFIG=$PWD/k3s.yaml"
echo
echo "and start to use the cluster:"
echo  "$ kubectl get nodes"
echo